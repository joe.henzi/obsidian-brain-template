---
created: <% tp.file.creation_date("") %>
id: <% tp.file.creation_date("NNYYYY\QQMMDDdwwX") %>
original-folder: <% tp.file.folder() %>
title: <% tp.system.prompt("Name Me", "Front Matter Title ...") %>
type: <% tp.system.suggester(["Bookmark", "Date", "Event", "Goal", "History", "Idea", "Keepsake", "Me", "Meta", "News", "Other Attachment", "People", "Problem", "Questions", "Solutions", "Tech", "Things", "Issue"],["Bookmark", "Date", "Event", "Goal", "History", "Idea", "Keepsake", "Me", "Meta", "News", "Other Attachment", "People", "Problem", "Questions", "Solutions", "Tech", "Things", "Issue"]) %>
template-startedEditTracking: 2022-09-18T02:10:34-04:00
template-updated: 2022-09-27 12:25 PM
updated: 2022-09-27 03:46 PM
---
<% tp.system.prompt("Intro", "Definition and the description of the note goes right here ...") %>

## Details

<% tp.file.cursor() %>

----

## #BackMatter 

### Article Actions



### Editor's Note



### Universal Tags

#Y<% tp.file.creation_date("YYYY") %>M<% tp.file.creation_date("MM") %>


