---
title: Template Playground
type: Tech
hasSubtypes: Event
---
## 2022-09-04 13:07

### Attempt - [[Journal Templates]]

*Am creating a template for each journal section - intent is for each file to be created using the right template per section, merge in the correct variables, including the dates that drive the correct links and field-links in #BackMatter/Ontology*

#### Link to File Create

First full attempt, created *this* document to support testing. 

- **Create new file**
- **Use [[Template - Daily Journal - First Impressions]] as the basis**
- **Use file name and path** (per notes in [[First Impression 124541 PM]] @ [[First Impression 124541 PM#^76c46b]] in the #Proposed/Filestructure tagged portion of the list
	- Folder & File Format: `/Daily Notes/H2/H3/....[[YYYY-MM-DD hh:MM A]]`
	- [x] [[Update Style Guide - Daily Note Folder and File Format Rules]] (@[[2022-09-15]] 17:17) [isRelated::[[My Style Guide - PKM and brain2]]] ✅ 2022-09-15
- **Note the brackets for the [[Wiki Links]] are APPLIED AND OUTSIDE THE [[Templater]] CODE BLOCK**

Link: [[undefined]]

##### Source Markdown

```markdown
[[undefined]]
```

####  Retry with Await

[[2022-09-04 8]]

##### Results - Outcome

The [[JS await]] keyword combined with the [[Templater Execution Command]] doesn't create the file (with template) upon click - it creates the output file *when the master template* is generated.

##### Results - Processing Steps

![[How Templater Treats Execution Commands]]

#### Button Code

*The "Button" code works using the [[Obsidian Plugin - Buttons (Official)]]*


	```button
	name Capture First Impressions
	type note(  ) template
	action Daily Journal - First Impressions
	templater true
	color blue
	```

OR 

```button
name Capture First Impressions
type note("FIT") template
action Daily Journal - First Impressions
templater true
color red
```



##### Breaks Template

Copied from [[Template - Daily Journal - First Impressions]]

```js
<% (await tp.file.rename( tp.file.creation_date("YYYY-MM-DD hhmm A") ) ) %>
<% (await tp.file.functions.move("Daily Notes/Journals/First Impressions") ) %>

```

Parent::[[Templater]]
Parent::[[Attempt - Journal Templates]]
Parent::[[Issue Template Attempt]]