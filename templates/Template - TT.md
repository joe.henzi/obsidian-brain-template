---
type: Journal
id: <% tp.file.creation_date("NNYYYY\QQMMDDdwwX") %>
title: Tonights Take on <% tp.date.now("YYYY-MM-DD")%> @ <% tp.date.now("hh:mm A") %>
timeOfDayRecorded: <% tp.date.now("hh:mm A")%>
startedEditTracking: 2022-09-17T23:20:00-04:00
updated: 2022-09-17T23:20:00-04:00
---
*Here are the thoughts and feelings for the journal notes for Tonight.*

## Journal Entry At <% tp.date.now("hh:mm A") %>

### Mood at <% tp.date.now("hh:mm A") %>

- @ [hasCurrentMood::[[<% tp.system.suggester(["Upbeat", "Sad", "Confused","Hopeful","Depressed","Cranky","Overjoyed","No Real Mood","Optimistic","Anxious","Calm","Ill or Sick","Mellow Smile","Nostalgic" ], ["Upbeat", "Sad", "Confused","Hopeful","Depressed","Cranky","Overjoyed","No Real Mood","Optimistic","Anxious","Calm","Ill or Sick","Mellow Smile","Nostalgic"]) %>]]]

- [Status::]
- [Feeling::]
- [TopOfMind::]
- [Desired::]
- [Did::]
- [ThinkingAbout::]
- [Wanting::]
- [Needing::]
- [FocusedOn::]
- [DesiredFocus::]
- [timestamp:: <% tp.date.now("YYYY-MM-DD hh:mm A") %>]
- [unixTimestamp:: <% tp.date.now("X") %>]
- [date:: <% tp.date.now("YYYY-MM-DD") %>]

### On My Mind



### Observations



### What's Happening?



### Looking Ahead At Value



### Action Items



----

## #BackMatter 

Parent::[[Daily Notes/<% tp.date.now("dddd, MMMM Do YYYY") %>]]

#DailyNote #JournalEntry #Y<% tp.file.creation_date("YYYY") %>M<% tp.file.creation_date("MM") %>

<% (await tp.file.rename( tp.file.creation_date("YYYY-MM-DD hhmm A") ) ) %>

<% (await tp.file.move("Daily Notes/Journal/Tonights Take/"+ tp.file.creation_date("YYYY-MM-DD hhmm A") ) ) %>
