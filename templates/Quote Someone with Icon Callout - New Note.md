---
type: Quote
main: false
publish: false
public: false
4QQ: false
---
*This is a noted quote - please add background information under it*

- [ ] [[#Attributes|Add Rating]]! (*Any number 1 through 100*)
- [ ] Provide [[#Background Details and Interest]]
- [ ] Sort me, [[#Attributes|update sorted key]]

```toc
style: bullet
title: "## Navigation"
varied_style: true
```

## Quote

>[!Quote] `<% tp.system.prompt("Quote?") %>`
> `*<% tp.system.prompt("Author?") %>*`

### Attributes

- File Name::`<% qcFileName %>`
- Created Date:: `<% tp.file.creation_date("YYYY-MM-DD HH:mm:ss") %>`
- WhoSaidThis:: #LinkMe
- Sorted:: false
- Tags
	- #Quoted
	-

### Background Details and Interest

*Here?*

## Old Matter

`<% let qcQuote = tp.system.prompt("Quote") %>`
`<% let qcAuthor = tp.system.prompt("Author")  %>`
`<% tp.system.prompt("Quote?") %>`
`<% await tp.file.rename(titleName)  %>`
`<% await tp.file.move("/An Inbox/" + titleName)  %>`
 `[[<% (await tp.system.suggester((item) => item.basename, app.vault.getMarkdownFiles())).basename %>]]`

---

## Other Templates:

![[_Index_of_templates]]