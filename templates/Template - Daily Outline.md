---
created: <% tp.file.creation_date("dddd Do MMMM YYYY HH:mm:ss") %>
type: Daily 
frequency: daily
startedEditTracking: 2022-09-17T22:59:13-04:00
updated: 2022-09-27 06:40 AM
---
## Opening Mood

>[!Info] [currentMoodIs::[[<% tp.system.suggester(["Upbeat", "Sad", "Confused","Hopeful","Depressed","Cranky","Overjoyed","No Real Mood","Optimistic","Anxious","Calm","Ill or Sick","Mellow Smile","Nostalgic" ], ["Upbeat", "Sad", "Confused","Hopeful","Depressed","Cranky","Overjoyed","No Real Mood","Optimistic","Anxious","Calm","Ill or Sick","Mellow Smile","Nostalgic"]) %>]]]

## Daily Image

<% tp.web.random_picture("400x400", "mountains, beach, space, universe, abstract, cool, impossible, basic, vintage, authentic, grace, beauty, sexy, perfect, kosher, godly, blessed, vacation, sun, life, health, values, gold, winning, views, vista, hope, sunshine, healing, happiness, nature, outdoors, goals, inspiration") %>

[intepretedDailyImageAs::template]

---

## Day Planner & Agenda

![[Recurring#Daily Tasks Reminders]]

### Overdue Tasks

```tasks
scheduled before today 
is not recurring
```

### Tasks Scheduled For Today

```tasks
scheduled today
is not recurring
```

### Tasks Due Today

```tasks
due on <% tp.file.creation_date("YYYY-MM-DD") %>
is not recurring
```

### Open Daily Notes and Journal Tasks

```tasks
not done
path includes Daily Notes
sort by urgency
```

----

## Looking Back Into the Future

```dataview
table
without id
notedToOneself as "As Once Noted...."
from "Daily Notes"
where notedToOneself
sort notedToOneself asc
limit 15
```

----

## Daily Journal

### Daily Quote

> *<% tp.web.daily_quote() %>*

### Daily Check-ins

```dataview
table 
WITHOUT ID
link(file.link, subtype + " @ " + timeOfDayRecorded) as "Note",
hasCurrentMood as "Mood",
Status as "Status",
Feeling as "Feeling",
TopOfMind as "Top of Mind",
Wanting as "Wanting",
Needing as "Needing"
from "Daily Notes/Journal"
where striptime(file.ctime) = striptime(this.file.ctime)
where !contains(file.path, "_")
flatten rows.all
SORT file.ctime ASC
```

```button
name Capture First Journal Entry
type note(FirstCheckDraft, split) template
action Template - Daily Journal - First Impressions
```

```button
name Capture Midday Journal Entry
type note(MiddayCheckDraft, split) template
action Template - Daily Journal - Midday Check
```

```button
name Capture Last Journal Entry
type note(LastCheckDraft, split) template
action Template - TT
```

## For Tomorrow

### Note to Your Future Self

>[!Help] *NOTE TO FUTURE SELF - Enter after the double-colon mark*
>[notedToOneself::]

### Future Tasks



---

## [[Ideas/Back Matter|Back Matter]]

### Neighbors

<< [prev:: [[Daily Notes/<% tp.date.now("dddd, MMMM Do YYYY", -1) %>]]] | [next::[[Daily Notes/<% tp.date.now("dddd, MMMM Do YYYY", 1) %>]]] >>

### Tags

#DailyNote #JournalEntry  #DayPlanner #Y<% tp.file.creation_date("YYYY") %>M<% tp.file.creation_date("MM") %>

## Today's [[brain2changelog]]

