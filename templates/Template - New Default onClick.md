---
created: <% tp.file.creation_date("") %>
id: <% tp.file.creation_date("NNYYYY\QQMMDDdwwX") %>
original-folder: <% tp.file.folder() %>
title: <% tp.system.prompt("Name Me", "right here ...") %>
type: <% tp.system.suggester(["Bookmark", "Date", "Event", "Goal", "History", "Idea", "Keepsake", "Me", "Meta", "News", "Other Attachment", "People", "Problem", "Questions", "Solutions", "Tech", "Things", "Issue"],["Bookmark", "Date", "Event", "Goal", "History", "Idea", "Keepsake", "Me", "Meta", "News", "Other Attachment", "People", "Problem", "Questions", "Solutions", "Tech", "Things", "Issue"]) %>
template-startedEditTracking-original: 2022-09-18T02:10:34-04:00
template-startedEditTracking: 2022-09-23 07:29 AM
template-updated: 2022-09-23 07:33 AM
updated: 2022-09-27 03:47 PM
---
<% tp.system.prompt("Intro Definition or Description", "right here ...") %>

## Details

<% tp.file.cursor() %>

----

## #BackMatter 

### Article Actions



### Editor's Note(s)



### Tags

#Y<% tp.file.creation_date("YYYY") %>M<% tp.file.creation_date("MM") %>

