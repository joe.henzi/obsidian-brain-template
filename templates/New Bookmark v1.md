---
date-bookmarked: <% tp.file.creation_date(format: string = "YYYY-MM-DD HH:mm A") %>
title: <% tp.system.prompt(prompt_text?: "Title of Link or Page?", default_value?: "Saved Link", throw_on_cancel: false) %>
type: link
---

## Link Details 

### URL

- <% tp.system.prompt(prompt_text?: "URL?", default_value?: "https://", throw_on_cancel: false) %>

### Description

- <% tp.system.prompt(prompt_text?: "Description?", default_value?: "A site that...", throw_on_cancel: false) %>

### Why Saved

- <% tp.system.prompt(prompt_text?: "What made you save it?", default_value?: "I want/need to...", throw_on_cancel: false) %>

----
[[Ideas/Back Matter]]

**Tags:** 

**Related Notes:**

- Link to a note?
