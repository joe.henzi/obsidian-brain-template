/*
THIS IS A GENERATED/BUNDLED FILE BY ESBUILD
if you want to view the source, please visit the github repository of this plugin
*/

var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __markAsModule = (target) => __defProp(target, "__esModule", { value: true });
var __commonJS = (cb, mod) => function __require() {
  return mod || (0, cb[Object.keys(cb)[0]])((mod = { exports: {} }).exports, mod), mod.exports;
};
var __export = (target, all) => {
  __markAsModule(target);
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __reExport = (target, module2, desc) => {
  if (module2 && typeof module2 === "object" || typeof module2 === "function") {
    for (let key of __getOwnPropNames(module2))
      if (!__hasOwnProp.call(target, key) && key !== "default")
        __defProp(target, key, { get: () => module2[key], enumerable: !(desc = __getOwnPropDesc(module2, key)) || desc.enumerable });
  }
  return target;
};
var __toModule = (module2) => {
  return __reExport(__markAsModule(__defProp(module2 != null ? __create(__getProtoOf(module2)) : {}, "default", module2 && module2.__esModule && "default" in module2 ? { get: () => module2.default, enumerable: true } : { value: module2, enumerable: true })), module2);
};
var __async = (__this, __arguments, generator) => {
  return new Promise((resolve, reject) => {
    var fulfilled = (value) => {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    };
    var rejected = (value) => {
      try {
        step(generator.throw(value));
      } catch (e) {
        reject(e);
      }
    };
    var step = (x) => x.done ? resolve(x.value) : Promise.resolve(x.value).then(fulfilled, rejected);
    step((generator = generator.apply(__this, __arguments)).next());
  });
};

// node_modules/obsidian-dataview/lib/index.js
var require_lib = __commonJS({
  "node_modules/obsidian-dataview/lib/index.js"(exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    require("obsidian");
    var getAPI3 = (app) => {
      var _a;
      if (app)
        return (_a = app.plugins.plugins.dataview) === null || _a === void 0 ? void 0 : _a.api;
      else
        return window.DataviewAPI;
    };
    var isPluginEnabled = (app) => app.plugins.enabledPlugins.has("dataview");
    exports.getAPI = getAPI3;
    exports.isPluginEnabled = isPluginEnabled;
  }
});

// main.ts
__export(exports, {
  default: () => SimpleNoteReviewPlugin
});
var import_obsidian6 = __toModule(require("obsidian"));
var import_obsidian_dataview2 = __toModule(require_lib());

// src/icon.ts
var import_obsidian = __toModule(require("obsidian"));
function addSimpleNoteReviewIcon() {
  (0, import_obsidian.addIcon)("simple-note-review-icon", `<path d="M66.7813 11.5556C51.5254 -3.85186 26.6953 -3.85186 11.4394 11.5556C-3.81314 26.9663 -3.81314 52.0397 11.4394 67.4505C25.0252 81.1679 46.1844 82.6375 61.4303 71.9261C61.7511 73.4592 62.4853 74.9221 63.666 76.1145L85.8834 98.5526C89.1211 101.816 94.353 101.816 97.5742 98.5526C100.809 95.2861 100.809 90.0022 97.5742 86.7457L75.3568 64.3009C74.1827 63.1185 72.7309 62.3737 71.2129 62.0497C81.8255 46.649 80.3704 25.283 66.7813 11.5556ZM59.7668 60.3664C48.377 71.8693 29.8404 71.8693 18.4539 60.3664C7.07069 48.8634 7.07069 30.146 18.4539 18.6431C29.8404 7.14343 48.377 7.14343 59.7668 18.6431C71.1567 30.146 71.1567 48.8634 59.7668 60.3664Z" fill="currentColor" stroke="currentColor" />
    <path d="M38.1915 23.3792C28.6514 23.3792 19.9999 28.6505 13.616 37.2126C13.0951 37.914 13.0951 38.894 13.616 39.5955C19.9999 48.1679 28.6514 53.4392 38.1915 53.4392C47.7316 53.4392 56.3831 48.1679 62.767 39.6058C63.288 38.9044 63.288 37.9244 62.767 37.2229C56.3831 28.6505 47.7316 23.3792 38.1915 23.3792ZM38.8759 48.9931C32.543 49.3954 27.3133 44.1241 27.7117 37.7181C28.0385 32.4364 32.2775 28.1554 37.5072 27.8253C43.84 27.423 49.0697 32.6943 48.6713 39.1004C48.3343 44.3717 44.0954 48.6527 38.8759 48.9931ZM38.5592 44.1035C35.1477 44.3201 32.3285 41.4833 32.5532 38.0378C32.7269 35.1907 35.0149 32.8903 37.834 32.7046C41.2456 32.488 44.0647 35.3248 43.84 38.7703C43.6561 41.6277 41.3682 43.9281 38.5592 44.1035Z" fill="currentColor" stroke="currentColor" />`);
}

// src/queue/queueService.ts
var import_obsidian2 = __toModule(require("obsidian"));

// src/dataview/dataviewFacade.ts
var import_obsidian_dataview = __toModule(require_lib());
var DataviewNotInstalledError = class extends Error {
  constructor() {
    super();
    this.message = "Dataview plugin not installed. To use Simple Note Review, Dataview plugin is needed.";
  }
};
var DataviewFacade = class {
  constructor() {
    this.isDataviewInstalled = false;
    try {
      this._api = (0, import_obsidian_dataview.getAPI)();
      if (this._api)
        this.isDataviewInstalled = true;
    } catch (error) {
      throw new DataviewNotInstalledError();
    }
  }
  pages(query) {
    this.throwIfDataviewNotInstalled();
    return this._api.pages(query);
  }
  page(filepath) {
    this.throwIfDataviewNotInstalled();
    return this._api.page(filepath);
  }
  throwIfDataviewNotInstalled() {
    if (!this.isDataviewInstalled)
      throw new DataviewNotInstalledError();
  }
};

// src/queue/dataviewService.ts
var DataviewService = class {
  constructor() {
    this._dataviewApi = new DataviewFacade();
  }
  getQueueFiles(queue) {
    const query = this.getOrCreateBaseDataviewQuery(queue);
    try {
      return this._dataviewApi.pages(query);
    } catch (error) {
      if (error instanceof DataviewNotInstalledError) {
        throw error;
      } else {
        throw new DataviewQueryError(`Query "${query}" contains errors. Please check settings for the current queue.`);
      }
    }
  }
  getOrCreateBaseDataviewQuery(queue) {
    if (queue.dataviewQuery && queue.dataviewQuery != "")
      return queue.dataviewQuery;
    let tags = "";
    let folders = "";
    if (queue.tags) {
      tags = queue.tags.map((p) => {
        if (p[0] !== "#")
          return "#" + p;
        return p;
      }).join(` ${queue.tagsJoinType || "or"} `);
    }
    if (queue.folders) {
      folders = queue.folders.join(" or ");
    }
    if (tags && folders)
      return `(${tags}) ${queue.foldersToTagsJoinType || "or"} (${folders})`;
    if (tags)
      return tags;
    return folders;
  }
  getPageFromPath(path) {
    return this._dataviewApi.page(path);
  }
};

// src/joinLogicOperators.ts
var JoinLogicOperators;
(function(JoinLogicOperators2) {
  JoinLogicOperators2["AND"] = "and";
  JoinLogicOperators2["OR"] = "or";
})(JoinLogicOperators || (JoinLogicOperators = {}));

// src/queue/queueInfoService.ts
var QueueInfoService = class {
  constructor(_dataviewService) {
    this._dataviewService = _dataviewService;
  }
  updateQueueStats(queue) {
    const pages = this._dataviewService.getQueueFiles(queue);
    queue.stats = {
      totalCount: pages.length,
      notRewiewedCount: pages.where((p) => !p.reviewed).length,
      reviewedLastSevenDaysCount: pages.where((p) => p.reviewed > this.getDateOffsetByNDays(7)).length,
      reviewedLastThirtyDaysCount: pages.where((p) => p.reviewed > this.getDateOffsetByNDays(30)).length
    };
  }
  updateQueueDisplayNameAndDescription(queue) {
    queue.displayName = this.getQueueDisplayName(queue);
    queue.description = this.getQueueDescription(queue);
  }
  getQueueDisplayName(queue) {
    if (queue.name && queue.name !== "") {
      return queue.name;
    }
    const alias = this._dataviewService.getOrCreateBaseDataviewQuery(queue);
    return alias && alias != "" ? alias : "blank queue";
  }
  getQueueDescription(queue) {
    var _a, _b, _c, _d, _e;
    let desc = "matches notes that ";
    if (queue.dataviewQuery && queue.dataviewQuery !== "") {
      desc += `are matched with dataviewJS query ${queue.dataviewQuery}`;
      return desc;
    }
    if (((_a = queue.tags) == null ? void 0 : _a.length) === 0 && ((_b = queue.folders) == null ? void 0 : _b.length) === 0) {
      return "matches all notes";
    }
    if (queue.tags && ((_c = queue.tags) == null ? void 0 : _c.length) > 0) {
      desc += `contain ${queue.tagsJoinType === JoinLogicOperators.AND ? "all" : "any"} of these tags: ${queue.tags.join(", ")}`;
      if (queue.folders && ((_d = queue.folders) == null ? void 0 : _d.length) > 0)
        desc += ` ${queue.foldersToTagsJoinType === JoinLogicOperators.AND ? "and" : "or"} `;
    }
    if (queue.folders && ((_e = queue.folders) == null ? void 0 : _e.length) > 0) {
      desc += `are inside any of these folders (including nested folders): ${queue.folders.join(", ")}`;
    }
    return desc;
  }
  getDateOffsetByNDays(days) {
    const today = new Date();
    const offsetDate = new Date();
    offsetDate.setDate(today.getDate() - days);
    return offsetDate;
  }
};

// src/queue/queueService.ts
var QueueEmptyError = class extends Error {
  constructor() {
    super(...arguments);
    this.message = "Queue is empty";
  }
};
var DataviewQueryError = class extends Error {
};
var QueueService = class {
  constructor(_app, _plugin) {
    this._app = _app;
    this._plugin = _plugin;
    this._dataviewService = new DataviewService();
    this._queueInfoService = new QueueInfoService(this._dataviewService);
  }
  updateQueueDisplayNames() {
    this._plugin.settings.queues.forEach((q) => this.updateQueueDisplayNameAndDescription(q));
  }
  updateQueueDisplayNameAndDescription(queue) {
    this._queueInfoService.updateQueueDisplayNameAndDescription(queue);
  }
  updateQueueStats(queue) {
    this._queueInfoService.updateQueueStats(queue);
  }
  reviewNote(_0) {
    return __async(this, arguments, function* (note, queue = this._plugin.settings.currentQueue) {
      if (!(note instanceof import_obsidian2.TFile))
        return;
      try {
        yield this.setMetadataValueToToday(note);
      } catch (error) {
        this._plugin.showNotice(error.message);
      }
      if (this._plugin.settings.openNextNoteAfterReviewing) {
        yield this.openNextFile(queue);
      }
    });
  }
  openNextFile(queue) {
    return __async(this, null, function* () {
      const filePath = this.getNextFilePath(queue);
      const abstractFile = this._app.vault.getAbstractFileByPath(filePath);
      yield this._app.workspace.getLeaf().openFile(abstractFile);
    });
  }
  setMetadataValueToToday(file) {
    return __async(this, null, function* () {
      const todayString = new Date().toISOString().slice(0, 10);
      yield this.changeOrAddMetadataValue(file, todayString);
      this._plugin.showNotice(`Marked note "${file.path}" as reviewed today.`);
    });
  }
  getNextFilePath(queue) {
    const pages = this._dataviewService.getQueueFiles(queue);
    const sorted = pages.sort((x) => x[this._plugin.settings.fieldName], "asc").array();
    if (sorted.length > 0) {
      const firstNoteIndex = this._plugin.settings.openRandomNote ? ~~(Math.random() * sorted.length) : 0;
      const firstInQueue = sorted[firstNoteIndex]["file"]["path"];
      if (sorted.length === 1) {
        return firstInQueue;
      }
      const nextInQueue = sorted[1]["file"]["path"];
      return this.pathEqualsCurrentFilePath(firstInQueue) ? nextInQueue : firstInQueue;
    }
    throw new QueueEmptyError();
  }
  pathEqualsCurrentFilePath(path) {
    return path === this._app.workspace.getActiveFile().path;
  }
  changeOrAddMetadataValue(file = null, value) {
    return __async(this, null, function* () {
      const newFieldValue = `${this._plugin.settings.fieldName}: ${value}`;
      const fileContentSplit = (yield this._app.vault.read(file)).split("\n");
      const page = this._dataviewService.getPageFromPath(file.path);
      if (!page[this._plugin.settings.fieldName]) {
        if (fileContentSplit[0] !== "---") {
          fileContentSplit.unshift("---");
          fileContentSplit.unshift("---");
        }
        fileContentSplit.splice(1, 0, newFieldValue);
        yield this._app.vault.modify(file, fileContentSplit.join("\n"));
        return;
      }
      const newContent = fileContentSplit.map((line) => {
        if (!line.startsWith(this._plugin.settings.fieldName))
          return line;
        return newFieldValue;
      });
      yield this._app.vault.modify(file, newContent.join("\n"));
    });
  }
};

// src/queue/selectQueueModal.ts
var import_obsidian3 = __toModule(require("obsidian"));
var SelectQueueModal = class extends import_obsidian3.SuggestModal {
  constructor(_app, _plugin) {
    super(_app);
    this._app = _app;
    this._plugin = _plugin;
  }
  getSuggestions(query) {
    this.setPlaceholder("Select a queue to start reviewing notes");
    return this._plugin.settings.queues.filter((q) => {
      if (query === "") {
        return true;
      }
      const name = q.displayName;
      if (!name || name === "") {
        return false;
      }
      return name.toLowerCase().includes(query.toLowerCase());
    });
  }
  renderSuggestion(queue, el) {
    el.createDiv({ text: queue.displayName });
    el.createEl("small", { text: queue.description }).style.opacity = "60%";
  }
  onChooseSuggestion(queue, evt) {
    return __async(this, null, function* () {
      try {
        this._plugin.settings.currentQueue = queue;
        this._plugin.saveSettings();
        this._plugin.showNotice(`Set current queue to ${queue.displayName}.`);
        yield this._plugin.service.openNextFile(queue);
      } catch (error) {
        this._plugin.showNotice(error.message);
        this.open();
      }
    });
  }
};

// src/settings/pluginSettings.ts
var DefaultSettings = class {
  constructor() {
    this.fieldName = "reviewed";
    this.queues = [];
    this.currentQueue = null;
    this.openNextNoteAfterReviewing = true;
    this.openRandomNote = false;
  }
};

// src/settings/settingsTab.ts
var import_obsidian5 = __toModule(require("obsidian"));

// src/queue/IQueue.ts
var EmptyQueue = class {
  constructor(id) {
    this.id = id;
  }
};

// src/queue/queueInfoModal.ts
var import_obsidian4 = __toModule(require("obsidian"));
var QueueInfoModal = class extends import_obsidian4.Modal {
  constructor(app, queue, service) {
    super(app);
    this.queue = queue;
    this.service = service;
  }
  onOpen() {
    this.service.updateQueueStats(this.queue);
    const { contentEl } = this;
    contentEl.createEl("h3", { text: `Queue "${this.queue.displayName}"` });
    contentEl.createDiv({ text: `This query ${this.queue.description}.` });
    contentEl.createEl("br");
    contentEl.createEl("p", { text: `Total notes: ${this.queue.stats.totalCount}` });
    contentEl.createEl("p", { text: `Reviewed in last 7 days: ${this.queue.stats.reviewedLastSevenDaysCount}` });
    contentEl.createEl("p", { text: `Reviewed in last 30 days: ${this.queue.stats.reviewedLastThirtyDaysCount}` });
    contentEl.createEl("p", { text: `Not reviewed yet: ${this.queue.stats.notRewiewedCount}` });
  }
  onClose() {
    const { contentEl } = this;
    contentEl.empty();
  }
};

// src/settings/settingsTab.ts
var SimpleNoteReviewPluginSettingsTab = class extends import_obsidian5.PluginSettingTab {
  constructor(_plugin, app) {
    super(app, _plugin);
    this._plugin = _plugin;
  }
  refresh() {
    this.display();
  }
  display() {
    const { containerEl } = this;
    containerEl.empty();
    containerEl.createEl("h2", { text: "Simple Note Review Settings" });
    new import_obsidian5.Setting(containerEl).setName("Open next note in queue after reviewing a note").setDesc("After marking note as reviewed, automatically open next note in queue.").addToggle((toggle) => {
      toggle.setValue(this._plugin.settings.openNextNoteAfterReviewing).onChange((value) => {
        this._plugin.settings.openNextNoteAfterReviewing = value;
        this._plugin.saveSettings;
      });
    });
    new import_obsidian5.Setting(containerEl).setName("Open random note for review").setDesc("When reviewing, open random note from queue, instead of note with earliest review date.").addToggle((toggle) => {
      toggle.setValue(this._plugin.settings.openRandomNote).onChange((value) => {
        this._plugin.settings.openRandomNote = value;
        this._plugin.saveSettings;
      });
    });
    containerEl.createEl("h3", { text: "Queues" });
    containerEl.createEl("div", { text: "A queue is a list of notes waiting for review." });
    this._plugin.settings && this._plugin.settings.queues && this._plugin.settings.queues.forEach((queue) => {
      this._plugin.service.updateQueueDisplayNameAndDescription(queue);
      const header = new import_obsidian5.Setting(containerEl);
      const baseSettingIconContainer = createSpan({ cls: "simple-note-review-collapse-icon" });
      header.setHeading();
      header.setClass("queue-heading");
      const updateHeader = (text) => {
        header.setName(`Queue "${text}"`);
        (0, import_obsidian5.setIcon)(baseSettingIconContainer, "right-chevron-glyph");
        header.nameEl.prepend(baseSettingIconContainer);
      };
      updateHeader(queue.displayName);
      header.addExtraButton((cb) => {
        cb.setIcon("info").setTooltip("Queue info & stats").onClick(() => {
          new QueueInfoModal(this.app, queue, this._plugin.service).open();
        });
      });
      header.addExtraButton((cb) => {
        cb.setIcon("trash").setTooltip("Delete queue").onClick(() => __async(this, null, function* () {
          console.log("deleting queue with id", queue.id, this._plugin.settings.queues);
          this._plugin.settings.queues = this._plugin.settings.queues.filter((q) => q.id !== queue.id);
          yield this._plugin.saveSettings();
          this.refresh();
        }));
      });
      const settingBodyEl = containerEl.createDiv({ cls: ["setting-body", "is-collapsed"] });
      header.settingEl.addEventListener("click", (e) => {
        settingBodyEl.toggleClass("is-collapsed", !settingBodyEl.hasClass("is-collapsed"));
        baseSettingIconContainer.toggleClass("rotated90", !baseSettingIconContainer.hasClass("rotated90"));
      });
      const nameSetting = new import_obsidian5.Setting(settingBodyEl);
      nameSetting.setName("Name");
      nameSetting.setDesc("If omitted, name will be created from tags/folders.");
      nameSetting.addText((textField) => {
        textField.setValue(queue.name).setPlaceholder(queue.displayName).onChange((value) => {
          if (value === queue.name) {
            return;
          }
          queue.name = value != "" ? value : null;
          this._plugin.saveSettings();
          if (value == "") {
            textField.setPlaceholder(queue.displayName);
          }
          updateQueueDisplayName();
        });
      });
      const tagsSetting = new import_obsidian5.Setting(settingBodyEl);
      tagsSetting.setName("Tags");
      tagsSetting.setDesc(`One or more tags, separated by comma. Queue will contain notes tagged with ${queue.tagsJoinType === JoinLogicOperators.AND ? "all" : "any"} of these. Example: #review, #knowledge`);
      tagsSetting.addTextArea((textArea) => {
        textArea.setValue(queue.tags ? queue.tags.join(",") : "").setPlaceholder("Tags").onChange((value) => {
          queue.tags = value != "" ? value.split(",").map((f) => f.trim()) : [];
          this._plugin.saveSettings();
          updateQueueDisplayName();
        });
      });
      const foldersSetting = new import_obsidian5.Setting(settingBodyEl);
      foldersSetting.setName("Folders");
      foldersSetting.setDesc(`One or more folder paths relative to vault root, surrounded by quotes and separated by comma. Queue will contain notes located in any of these. Example: "/notes", "/programming"`);
      foldersSetting.addTextArea((textArea) => {
        textArea.setValue(queue.folders ? queue.folders.join(",") : "").setPlaceholder("Folders").onChange((value) => {
          queue.folders = value != "" ? value.split(",").map((f) => f.trim()) : [];
          this._plugin.saveSettings();
          updateQueueDisplayName();
        });
      });
      const advancedSectionHeader = new import_obsidian5.Setting(settingBodyEl);
      advancedSectionHeader.setHeading();
      advancedSectionHeader.setName("Advanced");
      const advancedSectionIconContainer = createSpan({ cls: "simple-note-review-collapse-icon" });
      (0, import_obsidian5.setIcon)(advancedSectionIconContainer, "right-chevron-glyph");
      advancedSectionHeader.nameEl.prepend(advancedSectionIconContainer);
      const advancedSectionBodyEl = settingBodyEl.createDiv({ cls: ["setting-body-advanced", "is-collapsed"] });
      advancedSectionHeader.settingEl.addEventListener("click", (e) => {
        advancedSectionBodyEl.toggleClass("is-collapsed", !advancedSectionBodyEl.hasClass("is-collapsed"));
        advancedSectionIconContainer.toggleClass("rotated90", !advancedSectionIconContainer.hasClass("rotated90"));
      });
      const tagJoinTypeSetting = new import_obsidian5.Setting(advancedSectionBodyEl);
      tagJoinTypeSetting.setName("If tags are specified, match notes with:");
      tagJoinTypeSetting.addDropdown((dropdown) => {
        dropdown.addOption(JoinLogicOperators.OR, "any of the tags").addOption(JoinLogicOperators.AND, "all of the tags").setValue(queue.tagsJoinType || JoinLogicOperators.OR).onChange((value) => {
          queue.tagsJoinType = value;
          this._plugin.saveSettings();
          updateQueueDisplayName();
        });
      });
      const folderTagJoinTypeSetting = new import_obsidian5.Setting(advancedSectionBodyEl);
      folderTagJoinTypeSetting.setName("If folders and tags are specified, match notes with: ");
      folderTagJoinTypeSetting.addDropdown((dropdown) => {
        dropdown.addOption(JoinLogicOperators.OR, "specified tags OR in these folders").addOption(JoinLogicOperators.AND, "specified tags AND in these folders").setValue(queue.foldersToTagsJoinType || JoinLogicOperators.OR).onChange((value) => {
          queue.foldersToTagsJoinType = value;
          this._plugin.saveSettings();
          updateQueueDisplayName();
        });
      });
      const dataviewQuerySetting = new import_obsidian5.Setting(advancedSectionBodyEl);
      dataviewQuerySetting.setName("DataviewJS query");
      dataviewQuerySetting.setDesc(`DataviewJS-style query. If used, overrides Tags & Folders. Example: "(#knowledge and #review) or ('./notes')"`);
      dataviewQuerySetting.addTextArea((textArea) => {
        textArea.setValue(queue.dataviewQuery).setPlaceholder("DataviewJS query").onChange((value) => {
          queue.dataviewQuery = value;
          this._plugin.saveSettings();
          updateTagsFoldersSettingsAvailability(value);
          updateQueueDisplayName();
        });
      });
      const updateTagsFoldersSettingsAvailability = (dataviewJsQueryValue) => {
        const disableTagsFoldersSettings = dataviewJsQueryValue && dataviewJsQueryValue != "";
        if (disableTagsFoldersSettings) {
          tagsSetting.settingEl.style.opacity = "50%";
          foldersSetting.settingEl.style.opacity = "50%";
        } else {
          tagsSetting.settingEl.style.opacity = "100%";
          foldersSetting.settingEl.style.opacity = "100%";
        }
        tagsSetting.setDisabled(disableTagsFoldersSettings);
        foldersSetting.setDisabled(disableTagsFoldersSettings);
      };
      updateTagsFoldersSettingsAvailability(queue.dataviewQuery);
      const updateQueueDisplayName = () => {
        const debounced = (0, import_obsidian5.debounce)(() => {
          this._plugin.service.updateQueueDisplayNameAndDescription(queue);
          updateHeader(queue.displayName);
        }, 1e3);
        debounced();
      };
    });
    new import_obsidian5.Setting(containerEl).addButton((btn) => {
      btn.setButtonText("Add queue");
      btn.onClick(() => {
        this._plugin.settings.queues.push(new EmptyQueue(this._plugin.settings.queues.length > 0 ? Math.max(...this._plugin.settings.queues.map((q) => q.id)) + 1 : 1));
        this._plugin.saveSettings();
        this.refresh();
      });
    });
  }
};

// main.ts
var SimpleNoteReviewPlugin = class extends import_obsidian6.Plugin {
  constructor() {
    super(...arguments);
    this.service = new QueueService(this.app, this);
    this.openModalIconName = "simple-note-review-icon";
    this.markAsReviewedIconName = "checkmark";
  }
  onload() {
    return __async(this, null, function* () {
      this.app.workspace.onLayoutReady(() => {
        if (!this.dataviewIsInstalled()) {
          this.showNotice("Could not find Dataview plugin. To use Simple Note Review plugin, please install Dataview plugin first.");
        }
      });
      yield this.loadSettings();
      addSimpleNoteReviewIcon();
      this.service.updateQueueDisplayNames();
      this.addRibbonIcon(this.openModalIconName, "Simple Note Review", (evt) => {
        new SelectQueueModal(this.app, this).open();
      });
      this.addCommand({
        id: "open-modal",
        name: "Select Queue For Reviewing",
        callback: () => {
          new SelectQueueModal(this.app, this).open();
        }
      });
      this.addCommand({
        id: "set-reviewed-date",
        name: "Mark Note As Reviewed Today",
        editorCallback: (editor, view) => __async(this, null, function* () {
          yield this.service.reviewNote(view.file);
        })
      });
      this.registerEvent(this.app.workspace.on("editor-menu", (menu, editor, view) => {
        menu.addItem((item) => {
          item.setTitle("Mark Note As Reviewed Today").setIcon(this.markAsReviewedIconName).onClick(() => __async(this, null, function* () {
            yield this.service.reviewNote(view.file);
          }));
        });
      }));
      this.registerEvent(this.app.workspace.on("file-menu", (menu, file) => {
        menu.addItem((item) => {
          item.setTitle("Mark Note As Reviewed Today").setIcon(this.markAsReviewedIconName).onClick(() => __async(this, null, function* () {
            yield this.service.reviewNote(file);
          }));
        });
      }));
      this.addSettingTab(new SimpleNoteReviewPluginSettingsTab(this, this.app));
    });
  }
  onunload() {
  }
  loadSettings() {
    return __async(this, null, function* () {
      this.settings = Object.assign({}, new DefaultSettings(), yield this.loadData());
    });
  }
  saveSettings() {
    return __async(this, null, function* () {
      yield this.saveData(this.settings);
    });
  }
  showNotice(message) {
    new import_obsidian6.Notice(message);
  }
  dataviewIsInstalled() {
    return !!(0, import_obsidian_dataview2.getAPI)();
  }
};
